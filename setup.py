import os
import setuptools

APP_ROOT = os.path.dirname(__file__)
README = os.path.join(APP_ROOT, 'README.md')

setuptools.setup(
    name='osd',
    version='0.6',
    description="A fully featured notification daemon",
    long_description=open(README).read(),
    long_description_content_type='text/markdown',
    url="https://gitlab.com/pcapriotti/osd",
    author="Paolo Capriotti",
    author_email="paolo@capriotti.io",
    license="BSD3",
    classifiers=[
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3",
    ],
    packages=["osd"],
    scripts=["bin/osdd"]
)
