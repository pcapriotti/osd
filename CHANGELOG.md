## Version 0.6 (24 Apr 2021)

- Add API for killing notifications

## Version 0.5 (19 Feb 2019)

- Add LICENSE file

## Version 0.4 (09 Aug 2018)

- Adapt to python 3.7 changes

## Version 0.3 (31 Dec 2016)

- Add sound hint support

## Version 0.2 (26 Dec 2016)

- Make font configurable
- Do not display empty notifications

## Version 0.1 (25 Dec 2016)

- Initial release.
