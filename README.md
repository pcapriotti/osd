# osd

A fully featured notification daemon.

## Features

 - XDG compliant notifications (https://developer.gnome.org/notification-spec/)
 - Notifications whose body consists of a single percentage value are displayed as a progress bar
 - Fully configurable
